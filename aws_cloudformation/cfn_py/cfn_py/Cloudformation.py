import os
from utils import *
from botocore.exceptions import ClientError, WaiterError
import logging

log = logging.getLogger(__name__)


class Cloudformation:

    client = None
    delay = 30  # time in sec to wait between attempts
    max_attempts = 120  # Max num of attempts

    def __init__(self, session, region, stack_name):
        self.client = session.client("cloudformation", region_name=region)
        self.stack_name = stack_name

    @staticmethod
    def parameters(template_object, params):
        # Find parameters required in template and populate with parameters from config
        cft_parameters = []
        not_defined = []
        for parameter, values in template_object.get('Parameters', []).items():
            if parameter in params:
                cft_parameters.append({
                    'ParameterKey': parameter,
                    'ParameterValue': params[parameter],
                    'UsePreviousValue': False
                })
            else:
                # Mark Parameters as Not Defined if Default Value is not
                # provided in the CloudFormation template
                if 'Default' not in values:
                    not_defined.append(parameter)

        if not_defined:
            raise KeyError("Missing Required Parameters: %s" % ",".join(not_defined))

        log.info("Parameters:")
        for item in cft_parameters:
            log.info("%s=%s" % (item['ParameterKey'], item['ParameterValue']))

        return cft_parameters

    @staticmethod
    def tags(params, required_tags=['Environment']):
        # Populated Required tags with parameters
        missing_tags = []
        cft_tags = []
        for item in required_tags:
            if item not in params:
                # It tag key is not defined in parameters fail
                missing_tags.append(item)
            else:
                cft_tags.append({'Key': item, 'Value': params[item]})

        if missing_tags:
            raise KeyError("Missing Required Tags: %s" % ",".join(missing_tags))

        log.info("Tags:")
        for item in cft_tags:
            log.info("%s=%s" % (item['Key'], item['Value']))

        return cft_tags

    @staticmethod
    def capabilities(params):
        capabilities = []
        if 'capabilities' in params:
            capabilities = params['capabilities']

        if capabilities and not type(capabilities) == list:
            capabilities = [capabilities]

        return capabilities

    def validate_template(self, template_body):
        return self.client.validate_template(TemplateBody=template_body)

    def describe_stack(self):
        stack = self.client.describe_stacks(
            StackName=self.stack_name
        )
        return stack['Stacks'][0]

    def stack_status(self):
        # Enable function for custom stack name as well
        try:
            stack = self.describe_stack()
            return stack['StackStatus']
        except ClientError, e:
            if 'does not exist' in str(e):
                return 'DOES_NOT_EXIST'
            raise ClientError(e)

    def stack_outputs(self):
        stack = self.describe_stack()
        return stack['Outputs']

    def print_stack_events(self):
        events = self.client.describe_stack_events(
            StackName=self.stack_name
        )
        print "Stack Events:"
        for event in events['StackEvents']:
            reason = event['ResourceStatusReason'] if 'ResourceStatusReason' in event else ''
            print "%-20s %-30s %s %s" % (
                event['Timestamp'].strftime("%m/%d/%Y %H:%M%S"),
                event['ResourceStatus'],
                event['ResourceType'],
                reason
            )

        return

    def print_stack_outputs(self):
        outputs = self.stack_outputs()
        print "Stack Outputs:"
        for item in outputs:
            print "%-20s %s" % (item['OutputKey'], item['OutputValue'])

    def wait_stack_create(self):
        waiter = self.client.get_waiter('stack_create_complete')
        try:
            waiter.wait(
                StackName=self.stack_name,
                WaiterConfig={
                    'Delay': self.delay,
                    'MaxAttempts': self.max_attempts
                }
            )

            self.status()
            log.info("Created Stack: %s" % self.stack_name)
            return True
        except WaiterError as e:
            self.print_stack_events()
            raise Exception("Stack Creation Failed. %s" % str(e))

    def wait_stack_update(self):
        waiter = self.client.get_waiter('stack_update_complete')
        try:
            waiter.wait(
                StackName=self.stack_name,
                WaiterConfig={
                    'Delay': self.delay,
                    'MaxAttempts': self.max_attempts
                }
            )

            self.status()
            log.info("Updated Stack: %s" % self.stack_name)
            return True
        except WaiterError as e:
            self.print_stack_events()
            raise Exception("Stack Update Failed. %s" % str(e))

    def wait_stack_delete(self):
        waiter = self.client.get_waiter('stack_delete_complete')
        try:
            waiter.wait(
                StackName=self.stack_name,
                WaiterConfig={
                    'Delay': self.delay,
                    'MaxAttempts': self.max_attempts
                }
            )

            log.info("Deleted Stack: %s" % self.stack_name)
            return True
        except WaiterError as e:
            self.print_stack_events()
            raise Exception("Stack Delete Failed. %s" % str(e))


    def create(self, template_path, params, disable_rollback=False):
        stack_status = self.stack_status()

        if stack_status != 'DOES_NOT_EXIST':
            log.info("Stack Already Exists: %s, STATUS=%s" % (self.stack_name, stack_status))
            return True

        log.info("Validating Template...")
        template_as_string = file_string(template_path)
        template_as_object = file_object(template_path)
        # Validate template
        self.validate_template(template_as_string)

        log.info("Fetching required Template Parameters...")
        _parameters = self.parameters(template_as_object, params)

        log.info("Generating Tags...")
        _tags = self.tags(params)

        log.info("Creating Stack: %s..." % self.stack_name)
        self.client.create_stack(
            StackName=self.stack_name,
            TemplateBody=template_as_string,
            Parameters=_parameters,
            Tags=_tags,
            DisableRollback=disable_rollback,
            Capabilities=self.capabilities(params)
        )

        return self.wait_stack_create()

    def update(self, template_path, params):
        stack_name = self.stack_name
        stack_status = self.stack_status()

        if stack_status == 'DOES_NOT_EXIST':
            raise Exception("Stack Does not Exist. %s" % stack_name)

        if "IN_PROGRESS" in stack_status or 'ROLLBACK' in stack_status:
            raise Exception("Cannot Update Stack: %s. It is in %s state." % (self.stack_name, stack_status))

        log.info("Validating Template...")
        template_as_string = file_string(template_path)
        template_as_object = file_object(template_path)
        # Validate template
        self.validate_template(template_as_string)

        log.info("Validating Required Parameters...")
        _parameters = self.parameters(template_as_object, params)

        log.info("Generating Tags...")
        _tags = self.tags(params)

        log.info("Updating Stack: %s ..." % self.stack_name)
        self.client.update_stack(
            StackName=self.stack_name,
            TemplateBody=template_as_string,
            Parameters=_parameters,
            Tags=_tags,
            Capabilities=self.capabilities(params)
        )

        return self.wait_stack_update()

    def delete(self):
        stack_status = self.stack_status()

        if stack_status == 'DOES_NOT_EXIST':
            raise Exception("Stack Does not Exist. %s" % self.stack_name)

        log.info("Deleting Stack: %s ..." % self.stack_name)

        self.client.delete_stack(
            StackName=self.stack_name
        )

        return self.wait_stack_delete()

    def status(self):
        stack_status = self.stack_status()
        print "Stack Status: %s" % self.stack_status()
        if stack_status != 'DOES_NOT_EXIST':
            self.print_stack_events()
            self.print_stack_outputs()
