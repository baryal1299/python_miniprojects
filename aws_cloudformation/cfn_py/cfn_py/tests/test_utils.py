import unittest

from cfn_py import utils


class TestUtils(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.parameters_yaml = 'resources/test.properties.yaml'

    def test_file_string(self):
        _string = utils.file_string('resources/test.cft.yaml')
        self.assertIsInstance(_string, str)

    def test_file_object_a(self):
        _dict = utils.file_object('resources/test.cft.yaml')
        self.assertIsInstance(_dict, dict)

    def test_file_object_b(self):
        _dict = utils.file_object('resources/test.cft.yml')
        self.assertIsInstance(_dict, dict)

    def test_file_object_c(self):
        _dict = utils.file_object('resources/test.cft.json')
        self.assertIsInstance(_dict, dict)

    def test_file_object_d(self):
        self.assertRaises(IOError, lambda : utils.file_object('resources/test.cft.xml'))

    def test_load_parameters_yaml_default(self):
        params = utils.load_parameters_yaml(self.parameters_yaml, "dev")
        self.assertEqual(params['InstanceType'], "t2.micro")

    def test_load_parameters_yaml_env_overwrite(self):
        params = utils.load_parameters_yaml(self.parameters_yaml, "test")
        self.assertEqual(params['InstanceType'], "t2.medium")