import unittest
from botocore.exceptions import ClientError
from cfn_py import Cloudformation
from cfn_py import utils


class TestCloudFormation(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    def test_parameters(self):
        params = utils.load_parameters_yaml('resources/test.properties.yaml', 'dev')
        template_object = utils.file_object('resources/test.cft.json')
        parameters = Cloudformation.parameters(template_object, params)
        self.assertEquals(len(parameters), 1)

    def test_missing_parameters(self):
        params = {}
        template_object = utils.file_object('resources/test.cft.json')
        self.assertRaises(KeyError, lambda: Cloudformation.parameters(template_object, params))

    def test_tags(self):
        params = utils.load_parameters_yaml('resources/test.properties.yaml', 'dev')
        tags = Cloudformation.tags(params)
        self.assertEquals(len(tags), 1)

    def test_missing_tags(self):
        params = {}
        self.assertRaises(KeyError, lambda: Cloudformation.tags(params))