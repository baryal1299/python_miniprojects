import unittest
from botocore.exceptions import ProfileNotFound
from cfn_py import Auth


class TestUtils(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    def test_get_session_profile_not_found(self):
        auth = Auth(profile="does_not_exist")
        self.assertRaises(ProfileNotFound, lambda: auth.get_session())