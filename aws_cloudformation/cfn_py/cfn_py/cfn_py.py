#!/usr/bin/env python
import os
import sys
import argparse
from utils import *
from Auth import Auth
from Cloudformation import Cloudformation
import logging
import traceback
from botocore.exceptions import ProfileNotFound


# Logger Settings
logging.basicConfig(level=logging.INFO)
logging.getLogger("botocore").setLevel(logging.WARNING)
log = logging.getLogger(__name__)


def validate_outputs(outputs, verbose=False):
    """
    Run Validations on Cloudformation Outputs. Such as :
    - URL HTTP status OK
    :param verbose: Print Verbose Output of the Validations
    :param outputs: Cloudformation Outputs
    :return:
    """
    urls = [x['OutputValue'] for x in outputs if x['OutputValue'].startswith('http')]
    validate_urls(urls, verbose=verbose)


def create(cfn, args=None, params=None):
    cfn.create(args.cfn_template, params, disable_rollback=args.disable_rollback)
    validate_outputs(cfn.stack_outputs(), verbose=args.debug)
    return


def update(cfn, args=None, params=None):
    cfn.update(args.cfn_template, params)
    validate_outputs(cfn.stack_outputs(), verbose=args.debug)
    return


def status(cfn, args=None, params=None):
    cfn.status()
    validate_outputs(cfn.stack_outputs(), verbose=args.debug)
    return


def delete(cfn, args=None, params=None):
    return cfn.delete()


dispatcher = {
    "create": create,
    "update": update,
    "delete": delete,
    "status": status
}


def main():
    parser = argparse.ArgumentParser(description="Manage AWS Cloudformation Stack")
    rq = parser.add_argument_group("required arguments")
    rq.add_argument("--action", "-a", choices=dispatcher.keys(), required=True)
    rq.add_argument("--cfn-template", "-t", type=lambda x: parser_file_exists(parser, x), required=True, help="Cloudformation template path json/yaml")
    rq.add_argument("--properties-yaml", "-y", type=lambda x: parser_file_exists(parser, x), required=True, help="File Path to List of Properties in YAML file format")
    rq.add_argument("--login-profile", "-l", type=str, required=True, help="AWS Credentials/Profile to use")
    rq.add_argument("--region", "-r", type=str, required=True, help="AWS Region")
    rq.add_argument("--environment", "-e", type=str, required=False, help="Select Environment Specific Properties")

    parser.add_argument("-no-rollback", dest="disable_rollback", action="store_true", help="Disable Rollback")
    parser.add_argument("-debug", action="store_true", help="Debug")
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    # Fetch parameters from the yaml file
    params = load_parameters_yaml(args.properties_yaml, args.environment)

    # Make Sure required parameters are provided
    required = ['StackName', ]
    missing = []
    for param in required:
        if param not in params:
            missing.append(param)

    if missing:
        log.error("Missing Required Parameters. %s" % ",".join(missing))
        sys.exit(1)

    # Get AWS Session
    try:
        auth = Auth(profile=args.login_profile)
        session = auth.get_session()
    except ProfileNotFound:
        log.error("AWS Login Profile Not Found. %s " % args.login_profile)
        log.info("Please Run: $ aws configure --profile PROFILE_NAME to setup login profile.")
    except Exception as e:
        log.error("AWS Authentication Failed.")
        log.error(e)
        if args.debug:
            traceback.print_exc()
        sys.exit(1)

    # Execute AWS Api
    try:
        cfn = Cloudformation(session, args.region, params['StackName'])
        return dispatcher[args.action](cfn, args, params)
    except Exception as e:
        log.error("Unable to Complete Request.")
        log.error(e)
        if args.debug:
            traceback.print_exc()
        sys.exit(1)


if __name__ == '__main__':
    main()