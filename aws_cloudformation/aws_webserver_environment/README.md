# Aws WebServer Environment

The provided template in .json and .yaml format can be used to create AWS Environment which consists of the following:


The provided CloudFormation Template Creates following Resources

**AWS IAM InstanceProfile, Role and Managed Policy**

The EC2 Instance where the WebServer will be running must have permissions to put Logs to CloudWatch, so that access and error logs are available in CloudWatch. To allow EC2 Instance to put logs, create log streams etc. a role with set of permissions that allow access to these resource must be created, it can then be assumed by EC2 Instance when an Instance Profile is attached to the Instance when it starts.

**AWS CloudWatch Log Group**
CloudWatch Log Group where logs will be stored. 

**AWS SNS Topic**


**Auto Scaling Group**
Launch Configuration defines the config