# miniproject-ARYAL-BISHWASH

This project uses Cloudformation Template to provision AWS resources required to run scalable web server environment, as well as configures a Apache HTTP Server with static HTML page with the words "Automation for the People".

The Cloudformation templates can be found under directory - [aws_webserver_environment](aws_webserver_environment/)

The Resources created by the provided CloudFormation Templates are shown is the given diagram generated from CloudFormation Designer,

![Cloudformation Template Designer](screenshots/template.png)

Following Parameters are Required:

| Parameter Name | Parameter Value (Example) | Description |
|----------------|---------------------------|--------------|
|AutoScalingDesiredCapacity	|1	|Desired number of instances in Auto Scaling Group|
|AutoScalingMaxSize	|3	|Maximum number of instances in Auto Scaling Group|
|AutoScalingMinSize	|1	|Minimum number of instances in Auto Scaling Group|
|ImageId	|ami-0ff8a91507f77f867 |EC2 AMI Id, For the Latest Amazon Linux AMI Refer to [https://aws.amazon.com/amazon-linux-ami/](https://aws.amazon.com/amazon-linux-ami/). Select Appropriate AMI ID based on the AWS Region.|
|InstanceType	|t2.micro |EC2 instance type, Details about Instance Types can be found here [https://aws.amazon.com/ec2/instance-types/](https://aws.amazon.com/ec2/instance-types/)|
|KeyName	|bishwash.aws.keypair |Name of an existing EC2 KeyPair|
|SSHLocation	|0.0.0.0/0	|The IP address range that can be used to SSH to the EC2 instances|
|Subnets	|subnet-4032b527,subnet-5ce16200	|The list of SubnetIds in your Virtual Private Cloud (VPC)|
|VpcId	|vpc-d85f8ca2 |VpcId of your existing Virtual Private Cloud (VPC)|


The infrastructure consists of:

**IAM Role, Instance Profile, Managed Policy**

The EC2 Instance where the WebServer will be running must have permissions to put Logs to CloudWatch, so that access and error logs are available in CloudWatch. To allow EC2 Instance to put logs, create log streams etc. a role with set of permissions that allow access to these resource must be created, it can then be assumed by EC2 Instance when an Instance Profile is attached to the Instance when it starts.



**Security Groups**

There are two security groups created. 
1. Security Group to be applied to ALB, that allows any IP address to reach port 80.
2. Security Group to be applied to EC2 Instance, which allows access to 0-65535 port range from the ALB created as part of this stack. It also opens port 22 to provided IpCidr, to enable SSH access to the host.

**Sns Topic**

A Sns Topic is created, so that whenever the Auto Scaling group launches an instance, terminates, fails to launch or fails to terminate notification is sent this topic.

**CloudWatch Log Group**

Logs are essential component for monitoring and environment. Therefore, a log group is created where logs from WebServer created as part of this stack is stored separated by Instance Id.

**AutoScaling Group created using a Launch Configuration**

Instead of a single EC2 instance, for high availability and scalability i decided to provision collection of EC2 Instances as part of auto scaling group. Where min, max and desired counts, VPCZoneIdentifier are parameterized. Resources such as LaunchConfiguration, TargetGroups and SNSTopic are referenced from the same stack. 

AutoScaling group usage Launch Configuration to provide information needed to Launch an EC2 Instance. Where ImageId, InstanceType, Key Pair Name is parameterized while Security Groups and IamInstanceProfile is referenced from the current stack. 

In Launch Configuration using AWS::CloudFormation::Init, Apache HTTP Server is installed and Configured. Also, awslogs for is installed and configured to send logs from Apache HTTP access and error to CloudWatch, including the syslog /var/log/messages. The cfn-hup helper daemon that detects changes in metadata and allows update on stack update is also configured to run cnf-init.  

**Application Load Balancer, Listener and Target Group**

An application load balancer is created in the same subnets as Auto Scaling Group. The Security group which allows Enable HTTP access on the inbound port 80 is also attached.

Target group is created, with Protocol HTTP and port 80 as well timeout and threshold settings. VPCId is provided as a parameter.

Also, a Lister than forwards HTTP traffic on port 80 is created.

The application load Balancer uses the Listener that forwards HTTP traffic to Target Group referenced by AutoScalingGroup at port 80.  

## Infrastructure Provisioning

The infrastructure as defined in the CloudFormation template can be provisioned using AWS Web Console as well as provided Python package cfn-py. 

##### Assumptions
For this project we will assume that:
1. We have a VPC and Public Subnets already Available.
2. User account has all the privileges required for provisioning resources

## Using AWS Console

##### Pre-Requisites
1. A Key Pair is required. Please create a private Key Pair, which needs to be specified when launching an ec2 instance, so that you can SSH to the instance. [Creating a Key Pair Using Amazon EC2](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#having-ec2-create-your-key-pair) 


In AWS Account Navigate to Service "Cloudformation", which allows you to create and manage resources with templates.
 
Next, Click on "Create Stack"

You will have multiple options to Select a Template. 
1. You can Download the provided .json or .yaml template and select "Upload a template to Amazon S3", then provide the download file. It will upload the template to S3 bucket and the URL will be visible in the form field for template URL.
2. Or you can use Template Designer, by Clicking on "Design Template". It allows you to create or modify existing template. Next, select the "Template" tab on the bottom left corner of the page then copy and paste the provided template. On the top left corner of the page, Click on the icon with Up Arrow to Create Stack. This process will also upload the template to S3 Bucket and provide URL in the previous form. 
3. Since both process involves uploading template into S3 bucket, you can as well upload the template to S3 bucket first and provide the URL.  

Once you click on Next, you will be provided with a form where you specify the stack name and parameter values. You can overwrite the default values provided in AWS CloudFormation template as well.

Description for each parameter is provided in the form. Please refer to the following Screenshot for example. 

![Create Stack Screen Shot](screenshots/create_stack.png)


For the Latest Amazon Linux AMI Refer to [https://aws.amazon.com/amazon-linux-ami/](https://aws.amazon.com/amazon-linux-ami/). Select Appropriate AMI ID based on the AWS Region.

**Note:** The Provided Template currently supports Amazon Linux AMI Only. If any other AMI is to be used, template will need modifications to install packages that are already installed in Amazon Linux AMI, Such as aws-cfn-bootstrap and awslogs. Please, make sure you are using Amazon Linux 2 AMI, in the provided template the CloudWatch Logs agent configuration has to be modified to support the changes introduced in Amazon Linux 2 AMI.


Details about Instance Types can be found here [https://aws.amazon.com/ec2/instance-types/](https://aws.amazon.com/ec2/instance-types/)
. Select the appropriate Instance Type, as per the resource requirement for the application.
 
Once the form is completed click on Next.

Following Additional Options will be provided: 

Tags : As a good practice feel free to add Tags that will help you identify and filter resources. The resources that are part of CloudFormation stack will have tags to refer to the stack by default.

Permissions: The role or user account that will be used to create, modify or delete resources. The IAM Role or the User Account currently logged in must have permissions to provision all the resources provided in the CloudFormation template.

For this project, we will leave rest of the configurations as default and click on Next.  
 
In the Review page, towards the bottom of the page in Capabilities section, you must click on "I acknowledge that AWS CloudFormation might create IAM resources with custom names". The CloudFormation template provided creates IAM Managed Policy and Role which requires IAM Capabilities. It is better to separate IAM Resources from this template, so that it can be reused by more than one CloudFormation template, using parameter reference.  

Finally, Click on Create! 

Refresh the page, if you cannot see the list of CloudFormation Stack with Status - "CREATE_IN_PROGRESS". Select the Stack and go to "Events" to keep track of the progress.

The details about resources being created is describe here - [aws_webserver_environment](aws_webserver_environment/)

#### Validation
Once, the Stack is in Status - "CREATE_COMPLETE", Select the Stack Name in CloudFormation page and Click on "Outputs", you must have a URL Key with webserver URL from the Application Load Balancer similar to - http://WebSe-WebSe-A1RZBHAAG1HR-8379770.us-east-1.elb.amazonaws.com. Click on that URL, a new tab will open with page being served from your newly create WebServer Environment with text - "Automation for the People".

If the stack creation fails and status is not "CREATE_COMPLETE", Review the ClodFormation Stack "Events" for "Status Reason", which should provide details on why the creations failed.

#### WebServer and System Logs
Navigate to "Resources" tab, after you select the created Stack in CloudFormation. Find "WebServerLogGroup" in the list of Logical ID and Click on the Physical ID, that must take you to CloudWatch Service, where access and error logs from Apache HTTP Server as well as syslog from /var/log/messages is provided. Alternatively, you can navigate to thins page from CloudWatch Service -> Logs -> WebServer Log Group as well.

The Log Streams are prefixed with EC2 Instance ID, which helps identify specific EC2 instance running as part of Auto Scaling group and logs generated inside that particular Instance.

#### Alerts and Notifications
The CloudFormation template created a SNS Topic - "WebServerSns". 

Whenever the Auto Scaling group launches and instance, terminates, fails to launch or fails to terminate notification is sent to SNS topic - WebServerSns.

Additional alarms can be added based on CloudWatch Logs to send alerts to the same topic, which is not already done by the template. 

Users can subscribe to this SNS topic to receive notifications. [Subscribe to a Topic](https://docs.aws.amazon.com/sns/latest/dg/SubscribeTopic.html)

### Delete / Cleanup
To remove all the resources that were created as part of CloudFormation Stack, in CloudFormation Page, select the stack and click on "Actions" button on top left area of the page. Next, click on "Delete Stack".

All resources associated with this Stack will be removed.

## Using Python Script: cnf-py
While AWS Console itself is a great interface to create, update and remove CloudFormation Stack, the process involves manual steps which can be automated using [AWS SDK for Python (Boto3)](https://aws.amazon.com/sdk-for-python/). This enables to deploy CloudFormation Stack as part of CI/CD pipeline in reaction to version control triggers or post build triggers for infrastructure deployment along with latest application source code.

### Pre-Requisites
1. A Key Pair is required. If not already, Please create a private Key Pair, which needs to be specified when launching an ec2 instance, so that you can SSH to the instance. [Creating a Key Pair Using Amazon EC2](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#having-ec2-create-your-key-pair) 
2. User must have Programmatic access, Security Credentials : aws_access_key_id and aws_secret_access_key is required.
3. System must have Python 2.7 and pip installed.

**Note**: We will focus on Linux OS only. If Python 2.7 is available the script can be executed from windows environment as well.

#### Install cnf_py Package.
After you Clone this repository, follow the following steps:

```text
$ cd REPO_PATH/cfn_py
$ pip install .
Processing /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/cfn_py
Requirement already satisfied: PyYAML==3.13 in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from cfn-py==1.0) (3.13)
Requirement already satisfied: boto3==1.9.0 in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from cfn-py==1.0) (1.9.0)
Requirement already satisfied: botocore==1.12.0 in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from cfn-py==1.0) (1.12.0)
Requirement already satisfied: requests==2.19.1 in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from cfn-py==1.0) (2.19.1)
Requirement already satisfied: jmespath<1.0.0,>=0.7.1 in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from boto3==1.9.0->cfn-py==1.0) (0.9.3)
Requirement already satisfied: s3transfer<0.2.0,>=0.1.10 in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from boto3==1.9.0->cfn-py==1.0) (0.1.13)
Requirement already satisfied: docutils>=0.10 in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from botocore==1.12.0->cfn-py==1.0) (0.14)
Requirement already satisfied: python-dateutil<3.0.0,>=2.1; python_version >= "2.7" in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from botocore==1.12.0->cfn-py==1.0) (2.7.3)
Requirement already satisfied: urllib3<1.24,>=1.20 in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from botocore==1.12.0->cfn-py==1.0) (1.23)
Requirement already satisfied: idna<2.8,>=2.5 in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from requests==2.19.1->cfn-py==1.0) (2.7)
Requirement already satisfied: certifi>=2017.4.17 in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from requests==2.19.1->cfn-py==1.0) (2018.8.24)
Requirement already satisfied: chardet<3.1.0,>=3.0.2 in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from requests==2.19.1->cfn-py==1.0) (3.0.4)
Requirement already satisfied: futures<4.0.0,>=2.2.0; python_version == "2.6" or python_version == "2.7" in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from s3transfer<0.2.0,>=0.1.10->boto3==1.9.0->cfn-py==1.0) (3.2.0)
Requirement already satisfied: six>=1.5 in /bishwash/projects/github-bishwash-devops/miniproject-ARYAL-BISHWASH/venv/lib/python2.7/site-packages (from python-dateutil<3.0.0,>=2.1; python_version >= "2.7"->botocore==1.12.0->cfn-py==1.0) (1.11.0)
Building wheels for collected packages: cfn-py
  Running setup.py bdist_wheel for cfn-py ... done
  Stored in directory: /private/var/folders/15/1ns15s5d2xzbg1x5krn7x_n40000gn/T/pip-ephem-wheel-cache-wRs02V/wheels/fa/d8/6f/dd9742e57fbe39ffc4244ebf97ea6a5b6b118853428d6f35b3
Successfully built cfn-py
Installing collected packages: cfn-py
  Found existing installation: cfn-py 1.0
    Uninstalling cfn-py-1.0:
      Successfully uninstalled cfn-py-1.0
Successfully installed cfn-py-1.0

``` 

**NOTE:** Admin privilege or sudo access might be required to install the package in the system. Recommended to use Python virtualenv instead. 

The Package can also be built and distributed as Docker Image. Refer to the provided cfn_py/Dockerfile. For this project we will not go over the details on Docker.


**USAGE**
```text
$ cfn-py -help
usage: cfn-py [-h] --action {status,create,update,delete} --cfn-template
              CFN_TEMPLATE --properties-yaml PROPERTIES_YAML --login-profile
              LOGIN_PROFILE --region REGION [--environment ENVIRONMENT]
              [-no-rollback] [-debug]

Manage AWS Cloudformation Stack

optional arguments:
  -h, --help            show this help message and exit
  -no-rollback          Disable Rollback
  -debug                Debug

required arguments:
  --action {status,create,update,delete}, -a {status,create,update,delete}
  --cfn-template CFN_TEMPLATE, -t CFN_TEMPLATE
                        Cloudformation template path json/yaml
  --properties-yaml PROPERTIES_YAML, -y PROPERTIES_YAML
                        File Path to List of Properties in YAML file format
  --login-profile LOGIN_PROFILE, -l LOGIN_PROFILE
                        AWS Credentials/Profile to use
  --region REGION, -r REGION
                        AWS Region
  --environment ENVIRONMENT, -e ENVIRONMENT
                        Select Environment Specific Properties

```


#### Configure Properties YAML
Parameters required for CloudFormation template will be provided based on a YAML file. Similar to one provide below.

```text
$ cat REPO_PATH/aws_webserver_environment/properties.yml
StackName: WebServer
InstanceType: t2.micro
AutoScalingMinSize: 1
AutoScalingMaxSize: 3
AutoScalingDesiredCapacity: 1
capabilities: CAPABILITY_NAMED_IAM
Environments:
  dev:
    VpcId: vpc-d85f8ca2
    Subnets: subnet-4032b527, subnet-5ce16200
    ImageId: ami-0ff8a91507f77f867
    KeyName: bishwash.aws.keypair
  qa:
  prod:
```

Notice the YAML format, the KeyName maps directly to the Parameter Name in CloudFormation Template, with two additional fields - StackName and Environments.

We already discussed about the Parameters in CloudFormation template above.

StackName - Name of the Stack to Be Created

Environments - Stack could be deployed to multiple environments, the parameters are overwritten by environment specific parameters. When creating a stack, we will also specify the environment name, which dictates which values to use for that specific environment.

#### AWS Credentials 

We will use aws configure to generate ~/.aws/credentials file and refer to the profile for authorization in the script.

If you have not already installed awscli.
```text
$ pip install awscli
``` 

Next, configure aws
```text
$ aws configure --profile PROFILE_NAME
aws_access_key_id: *********
aws_secret_access_key: **********
Default region name: us-east-1
Default output format: json
```

### Create CloudFormation Stack
Please make sure all the pre-requisites are completed before running the create command. 

The cfn-hup Create Command Does the following in given order:
1. Read Properties from the Given YAML properties file
2. Makes Sure Required Parameters such as 'StackName' is provided in addition to CloudFormation Parameter
3. Establish AWS Session
4. Validate CloudFormation Template
5. Assign Parameters to CloudFormation Template, if missing fail.
6. Add Tags to CloudFormation Stack, for now it is only adding Environment tag.
7. Run Create Stack, then Wait till the Stack is Creation is completed.
8. Print Stack Events
9. Check if there are any values starting with 'http' in the outputs, if yes send GET Request to those URLS, and Display HTTP STATUS CODE.

```text
$ cd REPO_PATH/
$ cfn-py --login-profile bishwash.aws --cfn-template aws_webserver_environment/template.yaml --properties-yaml aws_webserver_environment/properties.yml --region us-east-1 --environment dev --action create
INFO:cfn_py.Cloudformation:Validating Template...
INFO:cfn_py.Cloudformation:Fetching required Template Parameters...
INFO:cfn_py.Cloudformation:Parameters:
INFO:cfn_py.Cloudformation:Subnets=subnet-4032b527, subnet-5ce16200
INFO:cfn_py.Cloudformation:VpcId=vpc-d85f8ca2
INFO:cfn_py.Cloudformation:AutoScalingMinSize=1
INFO:cfn_py.Cloudformation:AutoScalingMaxSize=3
INFO:cfn_py.Cloudformation:ImageId=ami-0ff8a91507f77f867
INFO:cfn_py.Cloudformation:KeyName=bishwash.aws.keypair
INFO:cfn_py.Cloudformation:AutoScalingDesiredCapacity=1
INFO:cfn_py.Cloudformation:InstanceType=t2.micro
INFO:cfn_py.Cloudformation:Generating Tags...
INFO:cfn_py.Cloudformation:Tags:
INFO:cfn_py.Cloudformation:Environment=dev
INFO:cfn_py.Cloudformation:Creating Stack: WebServer...
.....
```
Let it run for a while, it will take some time to create the resources. If interested, you can login to AWS Console to check on the events. 
  
Once Resource Creation is completed... 
```text
Stack Status: CREATE_COMPLETE
Stack Events:
09/14/2018 13:5133   CREATE_COMPLETE                AWS::CloudFormation::Stack 
09/14/2018 13:5131   CREATE_COMPLETE                AWS::AutoScaling::AutoScalingGroup 
09/14/2018 13:5130   CREATE_IN_PROGRESS             AWS::AutoScaling::AutoScalingGroup Received SUCCESS signal with UniqueId i-020cb3962c624a582
09/14/2018 13:5007   CREATE_IN_PROGRESS             AWS::AutoScaling::AutoScalingGroup Resource creation Initiated
09/14/2018 13:4958   CREATE_IN_PROGRESS             AWS::AutoScaling::AutoScalingGroup 
09/14/2018 13:4956   CREATE_COMPLETE                AWS::AutoScaling::LaunchConfiguration 
09/14/2018 13:4956   CREATE_IN_PROGRESS             AWS::AutoScaling::LaunchConfiguration Resource creation Initiated
09/14/2018 13:4955   CREATE_IN_PROGRESS             AWS::AutoScaling::LaunchConfiguration 
09/14/2018 13:4952   CREATE_COMPLETE                AWS::EC2::SecurityGroup 
09/14/2018 13:4952   CREATE_COMPLETE                AWS::IAM::InstanceProfile 
09/14/2018 13:4950   CREATE_IN_PROGRESS             AWS::EC2::SecurityGroup Resource creation Initiated
09/14/2018 13:4946   CREATE_COMPLETE                AWS::ElasticLoadBalancingV2::Listener 
09/14/2018 13:4945   CREATE_IN_PROGRESS             AWS::EC2::SecurityGroup 
09/14/2018 13:4945   CREATE_IN_PROGRESS             AWS::ElasticLoadBalancingV2::Listener Resource creation Initiated
09/14/2018 13:4945   CREATE_IN_PROGRESS             AWS::ElasticLoadBalancingV2::Listener 
09/14/2018 13:4942   CREATE_COMPLETE                AWS::ElasticLoadBalancingV2::LoadBalancer 
09/14/2018 13:4755   CREATE_COMPLETE                AWS::IAM::ManagedPolicy 
09/14/2018 13:4751   CREATE_IN_PROGRESS             AWS::IAM::InstanceProfile Resource creation Initiated
09/14/2018 13:4751   CREATE_IN_PROGRESS             AWS::IAM::InstanceProfile 
09/14/2018 13:4748   CREATE_IN_PROGRESS             AWS::IAM::ManagedPolicy Resource creation Initiated
09/14/2018 13:4748   CREATE_IN_PROGRESS             AWS::IAM::ManagedPolicy 
09/14/2018 13:4745   CREATE_COMPLETE                AWS::IAM::Role 
09/14/2018 13:4743   CREATE_COMPLETE                AWS::SNS::Topic 
09/14/2018 13:4741   CREATE_IN_PROGRESS             AWS::ElasticLoadBalancingV2::LoadBalancer Resource creation Initiated
09/14/2018 13:4739   CREATE_IN_PROGRESS             AWS::ElasticLoadBalancingV2::LoadBalancer 
09/14/2018 13:4737   CREATE_COMPLETE                AWS::EC2::SecurityGroup 
09/14/2018 13:4736   CREATE_IN_PROGRESS             AWS::EC2::SecurityGroup Resource creation Initiated
09/14/2018 13:4733   CREATE_COMPLETE                AWS::ElasticLoadBalancingV2::TargetGroup 
09/14/2018 13:4732   CREATE_COMPLETE                AWS::Logs::LogGroup 
09/14/2018 13:4732   CREATE_IN_PROGRESS             AWS::SNS::Topic Resource creation Initiated
09/14/2018 13:4732   CREATE_IN_PROGRESS             AWS::ElasticLoadBalancingV2::TargetGroup Resource creation Initiated
09/14/2018 13:4732   CREATE_IN_PROGRESS             AWS::Logs::LogGroup Resource creation Initiated
09/14/2018 13:4732   CREATE_IN_PROGRESS             AWS::IAM::Role Resource creation Initiated
09/14/2018 13:4732   CREATE_IN_PROGRESS             AWS::Logs::LogGroup 
09/14/2018 13:4732   CREATE_IN_PROGRESS             AWS::SNS::Topic 
09/14/2018 13:4732   CREATE_IN_PROGRESS             AWS::ElasticLoadBalancingV2::TargetGroup 
09/14/2018 13:4731   CREATE_IN_PROGRESS             AWS::IAM::Role 
09/14/2018 13:4731   CREATE_IN_PROGRESS             AWS::EC2::SecurityGroup 
09/14/2018 13:4727   CREATE_IN_PROGRESS             AWS::CloudFormation::Stack User Initiated
Stack Outputs:
URL                  http://WebSe-WebSe-P5RVJQWPTKAA-1775186908.us-east-1.elb.amazonaws.com
INFO:cfn_py.Cloudformation:Created Stack: WebServer
Validating URLs...
URL: http://WebSe-WebSe-P5RVJQWPTKAA-1775186908.us-east-1.elb.amazonaws.com HTTP_STATUS_CODE: 200 OK

```

**Validation**
After the Stack Creation is Complete, the Script searches for values that start with 'http' and send GET request to validate the response status. 
We validated that the Application Load Balancer Dns is reachable and returning HTTP 200 OK.

**Troubleshoot**
Disable Rollback: If Error occurs it is helpful to disable rollback of the CloudFormation stack, add --no-rollback flag to the command.

Debug: Add flag -debug to output debuggin informations.


### Update Cloudformation Stack
Similar to Create command, we can run update command to Update the Stack. It will go through exact same steps as create, only step added will be to check if stack exists before running updated.

To Run the command, change the action flag, --action update

```text
$ cd REPO_PATH/
$ cfn-py --login-profile bishwash.aws --cfn-template aws_webserver_environment/template.yaml --properties-yaml aws_webserver_environment/properties.yml --region us-east-1 --environment dev --action update
```

### Get Cloudformation Status
There is a status command available that print CloudFormation Stack status, as well as list of events. Then also runs status check in any URLs found in output values. The steps are same as the steps that were ran after Create Command completed.

To Run the command, change the action flag, --action status
```text
$ cd REPO_PATH/
$ cfn-py --login-profile bishwash.aws --cfn-template aws_webserver_environment/template.yaml --properties-yaml aws_webserver_environment/properties.yml --region us-east-1 --environment dev --action status
```

### Delete/Cleanup Stack
To Delete the CloudFormation Stack, please run the same command with flag --action set to delete. It will remove all the resources that were created as part of the CloudFormation Stack.

To Run the command, change the action flag, --action delete
```text
$ cd REPO_PATH/
$ cfn-py --login-profile bishwash.aws --cfn-template aws_webserver_environment/template.yaml --properties-yaml aws_webserver_environment/properties.yml --region us-east-1 --environment dev --action delete
```